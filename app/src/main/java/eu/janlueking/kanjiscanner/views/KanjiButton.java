package eu.janlueking.kanjiscanner.views;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

import eu.janlueking.kanjiscanner.activities.KanjiActivity;

/**
 * A button displaying a certain Kanji as its text that when clicked opens a KanjiActivity
 * with information about that particular Kanji
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class KanjiButton extends AppCompatButton {
    public KanjiButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * sets the Kanji that is displayed and linked to when the button is
     * clicked
     * @param kanji the kanji
     */
    public void setKanji(char kanji) {
        setText(Character.toString(kanji));
        setOnClickListener(view -> {
            final Intent intent = new Intent(getContext(), KanjiActivity.class);
            intent.putExtra("kanji", kanji);
            getContext().startActivity(intent);
        });
    }
}

package eu.janlueking.kanjiscanner.utils;

/**
 * helping class for doing often used string transformation tasks
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class StringFormatter {

    /**
     * returns a string with all strings from strings separated
     * with commas
     * @param   strings the strings
     * @return          comma separated strings
     */
    public static String getCommaSeparatedString(String[] strings) {
        if (strings.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(strings[0]);
        for (int i = 1; i < strings.length; i++) {
            sb.append(", ").append(strings[i]);
        }
        return sb.toString();
    }
}

package eu.janlueking.kanjiscanner.data;

/**
 * Container for all the data about one Kanji
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class Kanji {
    /** the kanji as an unicode character */
    public char kanji;
    /** the grade where japanese students typically learn this kanji */
    public String grade;
    /** the amount of strokes needed to write this kanji */
    public int stroke_count;
    /** the english meanings of this kanji */
    public String[] meanings;
    /** the kun readings */
    public String[] kun_readings;
    /** the on readings */
    public String[] on_readings;
    /** the name readings (they are not displayed) */
    public String[] name_readings;
    /** the jlpt level */
    public String jlpt;
    /** the unicode of the kanji */
    public String unicode;

    /**
     * @return  true if this object is filled with data, false otherwise
     */
    public boolean isFilled() {
        return stroke_count != 0;
    }
}

package eu.janlueking.kanjiscanner.data;

import org.json.JSONException;

import java.io.IOException;

/**
 * This class is not yet implemented and can not be used. This class is meant
 * as a future implementation of the KanjiData interface that provides the
 * data from an offline source instead of using online data
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class OfflineKanjiAPIData implements KanjiData {

    public OfflineKanjiAPIData() {

    }

    @Override
    public Kanji getKanjiData(char kanji) throws IOException, JSONException {
        return new Kanji();
    }

    @Override
    public KanjiWord[] getKanjiWordsData(char kanji) throws IOException, JSONException {
        return new KanjiWord[0];
    }
}

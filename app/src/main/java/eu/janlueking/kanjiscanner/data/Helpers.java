package eu.janlueking.kanjiscanner.data;

import android.content.Context;
import android.content.res.AssetManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Helper class that provides methods to check which kanji
 * are supported by this app or to filter out any characters
 * that are not supported
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class Helpers {
    /** every kanji that the kanjiApi can supply data for (sorted by unicode) */
    private static char[] availableKanji;
    /** the context for the assets */
    private static Context context;

    /**
     * initialises this class by obtaining a List of the supported Kanji. You can not use
     * any other public methods of this class or use any implementation of the KanjiData
     * interface before calling this
     * @param context the context for the asset manager
     */
    public static void init(Context context) {
        Helpers.context = context;
        AssetManager manager = context.getAssets();
        try {
            InputStream stream = manager.open("all_kanji.json");
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            reader.close();
            String jsonString = sb.toString();
            JSONArray jsonArray = new JSONArray(jsonString);
            availableKanji = new char[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                availableKanji[i] = jsonArray.getString(i).toCharArray()[0];
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @param   kanji   a character
     * @return          true if the character is a kanji and this app can provide information
     *                  about the kanji, false otherwise
     */
    public static boolean isAvailableKanji (char kanji) {
        if (availableKanji == null) {
            throw new RuntimeException(
                    "Helpers must be initialised before isAvailableKanji may be called");
        }
        return binarySearch(0, availableKanji.length - 1, kanji);
    }

    /**
     * @param   kanj    a list of characters
     * @return          the list of characters without any characters that are not supported
     *                  kanji
     */
    public static char[] filterAvailableKanji(char[] kanj) {
        ArrayList<Character> kanjiArr = new ArrayList<>();
        for (char k : kanj) {
            if (isAvailableKanji(k)) {
                kanjiArr.add(k);
            }
        }
        return charArrayListToCharArray(kanjiArr);
    }

    /**
     * @param   kanj    a string of characters
     * @return          the string of characters without any characters that are
     *                  not supported kanji
     */
    public static String filterAvailableKanji(String kanj) {
        char[] availableKanji = filterAvailableKanji(kanj.toCharArray());
        return  new String(availableKanji);
    }

    /**
     * @param   kanj    a list of characters
     * @return          the list of characters without any characters that are not
     *                  supported kanji and without duplicate characters
     */
    public static char[] filterUniqueAvailableKanji(char[] kanj) {
        kanj = filterAvailableKanji(kanj);
        return filterUniqueCharacters(kanj);
    }

    /**
     * @param kanj  a string of characters
     * @return      the string of characters without any characters that are not supported
     *              kanji and without duplicate characters
     */
    public static String filterUniqueAvailableKanji(String kanj) {
        char[] uniqueAvailableKanji = filterUniqueAvailableKanji(kanj.toCharArray());
        return new String(uniqueAvailableKanji);
    }

    /**
     * @param   chars   a list of characters
     * @return          the list of characters without any duplicate characters
     */
    public static char[] filterUniqueCharacters(char[] chars) {
        ArrayList<Character> unique = new ArrayList<>();
        for (char c : chars) {
            if (!unique.contains(c)) {
                unique.add(c);
            }
        }
        return charArrayListToCharArray(unique);
    }

    /**
     * returns a list of kanji from the asset with the given filename
     * @param listFileName  the filename of the asset
     * @return              all kanji from the list as a String
     */
    public static String getKanjiList(String listFileName) {
        AssetManager manager = context.getAssets();
        try {
            InputStream stream = manager.open(listFileName);
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            reader.close();
            String jsonString = sb.toString();
            JSONArray jsonArray = new JSONArray(jsonString);
            StringBuilder listKanji = new StringBuilder();
            for (int i = 0; i < jsonArray.length(); i++) {
                listKanji.append(jsonArray.getString(i).toCharArray()[0]);
            }
            return listKanji.toString();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * @return a String with all supported kanji
     */
    public static String getAvailableKanji() {
        return availableKanji.toString();
    }

    /**
     * @return the context Helpers was initialised with
     */
    public static Context getResourceContext() {
        return context;
    }

    /**
     * just a binarySearch implementation that finds a given character in a sorted list.
     * This is only used and can only be used for the availableKanji List, because the kanji
     * in the all_kanji.json file are sorted but any other list is not
     * @param   startIndex  the first index to search
     * @param   endIndex    the last index to search
     * @param   kanji       the kanji that is searched
     * @return              true if the kanji was found in the list, false otherwise
     */
    private static boolean binarySearch(int startIndex, int endIndex, char kanji) {
        if (startIndex > endIndex) {
            return false;
        }
        else {
            int index = (startIndex + endIndex) / 2;
            if (availableKanji[index] < kanji) {
                return binarySearch(index + 1, endIndex, kanji);
            }
            else if (availableKanji[index] > kanji) {
                return binarySearch(startIndex, index - 1, kanji);
            }
            else {
                return true;
            }
        }
    }

    /**
     * converts ArrayList<Character> to char[] with the equivalent elements
     * @param   charArrayList   the ArrayList<Character> to convert
     * @return                  the converted char[]
     */
    private static char[] charArrayListToCharArray(ArrayList<Character> charArrayList) {
        char[] charArray = new char[charArrayList.size()];
        for (int i = 0; i < charArrayList.size(); i++) {
            charArray[i] = charArrayList.get(i);
        }
        return charArray;
    }
}

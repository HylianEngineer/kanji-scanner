package eu.janlueking.kanjiscanner.data;

/**
 * this class is meant to store global settings made
 * in this app
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class Settings {
    /**
     * defines whether online or offline language files are used
     */
    public static boolean offlineData = false;

    public static long jpnDownloadId;
}

package eu.janlueking.kanjiscanner.data;

import eu.janlueking.kanjiscanner.R;

/**
 * A class that wraps all the data for one credit in the About menu and has a
 * static method that returns all credits. If you wanted to add a credit, you would
 * only need to edit this class' getCredits method and add string resources
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class Credit {
    private final int header;
    private final int subtext;
    private String url;

    public Credit(int header, int subtext) {
        this.header = header;
        this.subtext = subtext;
    }

    public Credit(int header, int subtext, String url) {
        this(header, subtext);
        this.url = url;
    }

    public int getHeader() {
        return header;
    }

    public int getSubtext() {
        return subtext;
    }

    public String getUrl() {
        return url;
    }

    /**
     * @return a list with all credits for this project
     */
    public static Credit[] getCredits() {
        Credit[] credits = new Credit[]{
            new Credit(R.string.edrdg_header,
                        R.string.edrdg_subtext,
                        "http://www.edrdg.org"),

            new Credit( R.string.kanji_api_header,
                        R.string.kanji_api_subtext,
                    "https://kanjiapi.dev"),

            new Credit( R.string.tesseract_header,
                        R.string.tesseract_subtext,
                    "https://github.com/rmtheis/tess-two"),

            new Credit( R.string.open_cv_header,
                        R.string.open_cv_subtext,
                    "https://opencv.org/android/"),

            new Credit( R.string.simple_crop_image_lib_header,
                        R.string.image_crop_subtext,
                    "https://github.com/biokys/cropimage"),

            new Credit( R.string.anyfanarts_header,
                        R.string.anyfanarts_subtext,
                    "https://www.instagram.com/anyfanarts")
        };
        return credits;
    }
}

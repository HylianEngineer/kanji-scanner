package eu.janlueking.kanjiscanner.data;

import android.content.Context;

import eu.janlueking.kanjiscanner.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * implementation of the KanjiData interface that obtains the data
 * from the online Source https://kanjiapi.dev
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class OnlineKanjiAPIData implements KanjiData {
    /** the context for getting string resources */
    Context context;
    
    public OnlineKanjiAPIData() {
        this.context = Helpers.getResourceContext();
    }
    
    @Override
    public Kanji getKanjiData(char kanji) throws IOException, JSONException {
        String urlString = "https://kanjiapi.dev/v1/kanji/" + kanji;
        try {
            String jSONString = getJSONString(urlString);
            JSONObject jsonObject = new JSONObject(jSONString);

            Kanji kanjiObject = new Kanji();

            kanjiObject.kanji = kanji;
            kanjiObject.grade = mapGrade(jsonObject);
            kanjiObject.stroke_count = jsonObject.getInt("stroke_count");
            kanjiObject.jlpt = mapJLPT(jsonObject);
            kanjiObject.unicode = jsonObject.getString("unicode");

            JSONArray meanings_array = jsonObject.getJSONArray("meanings");
            kanjiObject.meanings = jsonArrayToArray(meanings_array);

            JSONArray kun_readings_array = jsonObject.getJSONArray("kun_readings");
            kanjiObject.kun_readings = jsonArrayToArray(kun_readings_array);

            JSONArray on_readings_array = jsonObject.getJSONArray("on_readings");
            kanjiObject.on_readings = jsonArrayToArray(on_readings_array);

            JSONArray name_readings_array = jsonObject.getJSONArray("name_readings");
            kanjiObject.name_readings = jsonArrayToArray(name_readings_array);

            return kanjiObject;
        }
        catch (IOException ex) {
            throw ex;
        }
        catch (JSONException ex) {
            throw ex;
        }
    }

    @Override
    public KanjiWord[] getKanjiWordsData(char kanji) throws IOException, JSONException{
        String urlString = "https:/kanjiapi.dev/v1/words/" + kanji;
        String jSONString = getJSONString(urlString);
        JSONArray jsonArray = new JSONArray(jSONString);
        ArrayList<KanjiWord> words = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject wordObject = jsonArray.getJSONObject(i);
            JSONArray variants = wordObject.getJSONArray("variants");
            JSONArray meaningsObject = wordObject.getJSONArray("meanings");
            ArrayList<String> meanings = new ArrayList<>();
            for (int j = 0; j < meaningsObject.length(); j++) {
                JSONObject meaning = meaningsObject.getJSONObject(j);
                JSONArray glosses = meaning.getJSONArray("glosses");
                for (int k = 0; k < glosses.length(); k++) {
                    meanings.add(glosses.getString(k));
                }
            }
            for (int j = 0; j < variants.length(); j++) {
                JSONObject variant = variants.getJSONObject(j);
                String written = variant.getString("written");
                if (written.contains(Character.toString(kanji))) {
                    String pronounced = variant.getString("pronounced");
                    words.add(new KanjiWord(written, pronounced,
                            meanings.toArray(new String[0])));
                }
            }
        }
        return words.toArray(new KanjiWord[0]);
    }

    /**
     * makes an http request
     * @param   urlString   the url to make the request to
     * @return              a json String (the answer of the http request)
     */
    private String getJSONString(String urlString) throws IOException {
        URL url;
        try {
            url = new URL(urlString);
        }
        catch (MalformedURLException ex) {
            ex.printStackTrace();
            return null;
        }
        HttpURLConnection urlConnection;
        urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setReadTimeout(2000 /* milliseconds */);
        urlConnection.setConnectTimeout(4000 /* milliseconds */);
        urlConnection.setDoOutput(true);
        urlConnection.connect();
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line).append("\n");
        }
        br.close();
        return sb.toString();
    }

    /**
     * maps the grade Integer from the json file to a string representing what it actually
     * means according to the kanjiApi documentation
     * @param   gradeObject the jsonObject holding the grade attribute
     * @return              a string representation of the meaning of the number
     */
    private String mapGrade(JSONObject gradeObject) {
        int grade;
        try {
            grade = gradeObject.getInt("grade");
        }
        catch (JSONException ex) {
            return context.getString(R.string.advanced); // if value in xml is null
        }
        if (grade >= 1 && grade <= 6) {
            return grade+"";
        }
        if (grade == 8) {
            return "7-9";
        }
        if (grade == 9 || grade == 10) {
            return context.getString(R.string.jinmeiyo);
        }
        return context.getString(R.string.advanced);
    }

    /**
     * maps the jlpt Integer from the json file to a string representing what it actually
     * means according to the kanjiApi documentation. This is not 100% accurate
     * as this method is trying to vaguely map the old jlpt specification that the
     * kanjiApi provides to the newer jlpt specification. A better method would be
     * to check whether the kanji is part of one of the jlpt lists in my resources but
     * I have not yet found the time to do that.
     * @param   jlptObject  the jsonObject containing the jlpt Integer
     * @return              a string representation of the meaning of the number
     */
    private String mapJLPT(JSONObject jlptObject) {
        int jlpt;
        try {
            jlpt = jlptObject.getInt("jlpt");
        }
        catch (JSONException ex) {
            return context.getString(R.string.advanced); // if value in xml is null
        }
        if (jlpt >= 1 && jlpt <= 4) {
            return "N"+(jlpt+1);
        }
        return context.getString(R.string.advanced);
    }

    /**
     * reads a jsonArray with strings and puts every string into a string array
     * @param   jsonArray   a jsonArray containing only string arguments
     * @return              an array containing the strings of the json Array
     */
    private String[] jsonArrayToArray(JSONArray jsonArray) throws JSONException {
        String[] array = new String[jsonArray.length()];
        for (int i = 0; i < jsonArray.length(); i++) {
            array[i] = jsonArray.getString(i);
        }
        return array;
    }
}

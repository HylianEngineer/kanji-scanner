package eu.janlueking.kanjiscanner.data;

/**
 * Container for all the words written with one kanji
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class KanjiWord {
    /** how the word is written (using kanji) */
    public String written;
    /** how the word is pronounced (only using kana (syllable characters)) */
    public String pronounced;
    /** english meanings of the word */
    public String[] meanings;

    public KanjiWord() {

    }

    public KanjiWord(String written, String pronounced, String[] meanings) {
        this.written = written;
        this.pronounced = pronounced;
        this.meanings = meanings;
    }
}

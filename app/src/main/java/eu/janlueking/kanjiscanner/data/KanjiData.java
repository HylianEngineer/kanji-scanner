package eu.janlueking.kanjiscanner.data;

import org.json.JSONException;
import java.io.IOException;

/**
 * Interface for classes that provide data about kanji in the form
 * of Kanji and KanjiWord objects
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public interface KanjiData {
    /**
     * Get the Data about one kanji except the words encapsulated in the
     * kanji class
     * @param   kanji   The kanji for which to get the data
     * @return          a kanji Object filled with all the data
     */
    Kanji getKanjiData(char kanji) throws IOException, JSONException;

    /**
     * Gets a list of KanjiWord objects describing every Word that can be spelled
     * with a certain kanji
     * @param   kanji   The kanji for which to get the words
     * @return          A list of all the words containing the given kanji
     */
    KanjiWord[] getKanjiWordsData(char kanji) throws IOException, JSONException;

    /**
     * @return  an implementation of this interface. Returns the offline
     *          implementation if offline Data is available. Returns the online
     *          implementation instead.
     */
    static KanjiData getInstance() {
        if (Settings.offlineData) {
            return new OfflineKanjiAPIData();
        }
        else {
            return new OnlineKanjiAPIData();
        }
    }
}

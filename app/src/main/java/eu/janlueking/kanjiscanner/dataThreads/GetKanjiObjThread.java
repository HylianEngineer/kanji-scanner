package eu.janlueking.kanjiscanner.dataThreads;

import eu.janlueking.kanjiscanner.data.Kanji;
import eu.janlueking.kanjiscanner.data.KanjiData;

import org.json.JSONException;

import java.io.IOException;

/**
 * A thread for obtaining the Kanji Object asynchronously
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class GetKanjiObjThread extends Thread{
    GetKanjiObjListener callbackListener;
    char kanji;

    /**
     * @param listener the listener to receive a callback when the thread has finished
     * @param kanji the kanji for which to request the data
     */
    public GetKanjiObjThread(GetKanjiObjListener listener, char kanji) {
        this.callbackListener = listener;
        this.kanji = kanji;
    }

    /**
     * obtains the Kanji Object and invokes a callback on the
     * callbackListener with that Kanji Object or null if retrieving
     * the Kanji Object failed
     */
    @Override
    public void run() {
        KanjiData data = KanjiData.getInstance();
        try {
            Kanji kanjiObj = data.getKanjiData(kanji);
            callbackListener.getKanjiObjCallback(kanjiObj);
        }
        catch (IOException ex) {
            callbackListener.getKanjiObjCallback(null);
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            callbackListener.getKanjiObjCallback(null);
        }
    }
}

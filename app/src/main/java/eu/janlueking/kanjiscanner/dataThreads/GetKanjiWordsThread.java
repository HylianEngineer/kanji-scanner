package eu.janlueking.kanjiscanner.dataThreads;

import eu.janlueking.kanjiscanner.data.KanjiData;
import eu.janlueking.kanjiscanner.data.KanjiWord;

import org.json.JSONException;

import java.io.IOException;

/**
 * A thread for obtaining the KanjiWord list asynchronously
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class GetKanjiWordsThread extends Thread {
    GetKanjiWordsListener callbackListener;
    char kanji;

    /**
     * @param listener  the listener to receive a callback when the thread has finished
     * @param kanji     the kanji for which to request the words data
     */
    public GetKanjiWordsThread(GetKanjiWordsListener listener, char kanji) {
        this.callbackListener = listener;
        this.kanji = kanji;
    }

    /**
     * obtains the KanjiWord List and invokes a callback on the
     * callbackListener with that KanjiWord List or null if retrieving
     * the KanjiWord List failed
     */
    @Override
    public void run() {
        KanjiData data = KanjiData.getInstance();
        try {
            KanjiWord[] words = data.getKanjiWordsData(kanji);
            callbackListener.getKanjiWordsCallback(words);
        }
        catch (IOException ex) {
            callbackListener.getKanjiWordsCallback(null);
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            callbackListener.getKanjiWordsCallback(null);
        }
    }
}

package eu.janlueking.kanjiscanner.dataThreads;

import eu.janlueking.kanjiscanner.data.KanjiWord;

/**
 * Listener for when kanji words get returned
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public interface GetKanjiWordsListener {
    /**
     * This method gets invoked after the GetKanjiWordsThread
     * finishes getting the Kanji Object.
     * @param words The KanjiWords List or null if the thread failed to get the List
     */
    void getKanjiWordsCallback(KanjiWord[] words);
}

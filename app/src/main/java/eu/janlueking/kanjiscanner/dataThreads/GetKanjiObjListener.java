package eu.janlueking.kanjiscanner.dataThreads;

import eu.janlueking.kanjiscanner.data.Kanji;

/**
 * Listener for when a kanji object gets returned
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public interface GetKanjiObjListener {
    /**
     * This method gets invoked after the GetKanjiObjThread
     * finishes getting the Kanji Object.
     * @param kanji The Kanji Object or null if the thread failed to get the Object
     */
    void getKanjiObjCallback(Kanji kanji);
}

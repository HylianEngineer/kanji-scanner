package eu.janlueking.kanjiscanner.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ListView;

import eu.janlueking.kanjiscanner.listViewAdapter.CreditsListAdapter;
import eu.janlueking.kanjiscanner.data.Credit;
import eu.janlueking.kanjiscanner.R;

/**
 * An activity to credit all the work of other people that I used in this
 * project. The data for those credits is defined in the
 * data.Credit class
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("About");
        setContentView(R.layout.activity_about);

        ListView creditsListView = findViewById(R.id.credits_list_view);
        CreditsListAdapter adapter = new CreditsListAdapter(this, Credit.getCredits());
        creditsListView.setAdapter(adapter);
        creditsListView.setOnItemClickListener(((parent, view, position, id) -> {
            Credit item = adapter.getItem(position);
            openUrl(item.getUrl());
        }));
    }

    /**
     * opens the url in a web browser
     * @param url the url
     */
    private void openUrl(String url) {
        if(url == null) {
            return;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }
}

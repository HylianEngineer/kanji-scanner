package eu.janlueking.kanjiscanner.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

import eu.janlueking.kanjiscanner.R;

import java.io.IOException;
import java.io.InputStream;

/**
 * This Activity displays a short Tutorial for using the scan
 * functionality of this app optimally
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class TutorialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        getSupportActionBar().setTitle(R.string.tutorial);

        ImageView imageView1 = findViewById(R.id.tutorial_image1);
        ImageView imageView2 = findViewById(R.id.tutorial_image2);

        assignImageFromFileToImageView(imageView1, "tutorial_image.png");
        assignImageFromFileToImageView(imageView2, "tutorial_image2.png");
    }

    /**
     * Assigns the image with specified filename from the assets to the imageView
     * @param iView     the imageView to assign the image to
     * @param filename  the filename of the image asset
     */
    private void assignImageFromFileToImageView(ImageView iView, String filename) {
        try {
            InputStream stream = getAssets().open(filename);
            Bitmap image = BitmapFactory.decodeStream(stream);
            iView.setImageBitmap(image);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
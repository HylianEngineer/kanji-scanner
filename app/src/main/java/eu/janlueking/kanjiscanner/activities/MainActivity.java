package eu.janlueking.kanjiscanner.activities;

import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import eu.janlueking.kanjiscanner.data.Helpers;
import eu.janlueking.kanjiscanner.data.Settings;
import eu.janlueking.kanjiscanner.imageRecognition.ImageProcessor;
import eu.janlueking.kanjiscanner.R;
import eu.janlueking.kanjiscanner.imageRecognition.TessOCR;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import eu.janmuller.android.simplecropimage.CropImage;

/**
 * The Main Activity of this app that is opened when the app starts
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class MainActivity extends AppCompatActivity {
    /** intent Request Code for obtaining the image */
    private final int PIC_REQUEST = 1;
    /** intent Request Code for cropping the image */
    private final int CROP_REQUEST = 2;
    /** key for saving the filename in savedInstanceState */
    private final String FILENAME = "filename";
    /** key for saving the mostRecentOcrResult in savedInstanceState */
    private final String OCR_RESULT = "ocr_result";
    /** Object for doing optical character recognition */
    private TessOCR tessOCR;
    /** the uri of the image file */
    private Uri uri;
    /** the file where the image is saved */
    private File imageFile;
    /** the current bitmap of the image */
    private Bitmap kanjiScan;
    /** the result of the optical character recognition of the current image or null if the
     * image has not been scanned yet */
    private String mostRecentOcrResult;
    /** the thread that executes the optical character recognition */
    private Thread ocrThread;

    // global definition of Views for less duplicate code and shorter lines
    private Button selectImageButton;
    private Button scanImageButton;
    private Button searchButton;
    private Button browseListsButton;
    private Button cropButton;
    private Button aboutButton;
    private Button tutorialButton;
    private Button copyToSearchButton;
    private Button settingsButton;

    private TextInputEditText searchBar;
    private TextView imageDescriptionTextView;
    private ProgressBar scanProgressBar;
    private ImageView imageView;
    private CheckBox wordsCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();

        Helpers.init(this);
        System.loadLibrary("opencv_java3");

        setContentView(R.layout.activity_main);
        initViews();
        bindClickListeners();

        startTesseractAPI();

        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(downloadReceiver, filter);

        if (savedInstanceState == null) {
            createImageFile();
        }
    }

    /**
     * initialises the imageFile and the uri to save an image by creating
     * a file in the cache directory of this app
     */
    public void createImageFile() {
        final File imagePath = new File(getCacheDir(), "images");
        if (!imagePath.exists()) {
            imagePath.mkdirs();
        }
        imageFile = new File(imagePath, "img");

        // deletes the image in cache from last session
        imageFile.delete();

        uri = FileProvider.getUriForFile(this,
                "eu.janlueking.kanjiscanner.fileprovider", imageFile);
    }

    /**
     * initialises all the views
     */
    private void initViews() {
        selectImageButton = findViewById(R.id.select_image_button);
        scanImageButton = findViewById(R.id.scan_image_button);
        searchButton = findViewById(R.id.search_button);
        browseListsButton = findViewById(R.id.browse_lists_button);
        cropButton = findViewById(R.id.crop_image_button);
        aboutButton = findViewById(R.id.about_button);
        tutorialButton = findViewById(R.id.tutorial_button);
        copyToSearchButton = findViewById(R.id.copy_to_search_button);
        settingsButton = findViewById(R.id.settings_button);
        searchBar = findViewById(R.id.search_bar);
        imageDescriptionTextView = findViewById(R.id.image_description_textview);
        scanProgressBar = findViewById(R.id.scan_progress_bar);
        imageView = findViewById(R.id.image_view);
        wordsCheckBox = findViewById(R.id.words_checkbox);
    }

    /**
     * binds the click Listeners to the buttons
     */
    private void bindClickListeners() {
        selectImageButton.setOnClickListener(v -> {
            selectImageButtonClicked();
        });
        scanImageButton.setOnClickListener(v -> {
            scanImageButtonClicked();
        });
        searchButton.setOnClickListener(v -> {
            searchButtonClicked();
        });
        browseListsButton.setOnClickListener(v -> {
            browseListsButtonClicked();
        });
        cropButton.setOnClickListener(v -> {
            runCropImage();
        });
        aboutButton.setOnClickListener(v -> {
            aboutButtonClicked();
        });
        tutorialButton.setOnClickListener(v -> {
            tutorialButtonClicked();
        });
        copyToSearchButton.setOnClickListener( v -> {
            copyToSearchButtonClicked();
        });
        settingsButton.setOnClickListener(v -> {
            settingsButtonClicked();
        });
    }

    private void startTesseractAPI(){
        if (tessOCR != null){
            return;
        }
        new Thread(()->{
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/tessdata/jpn.traineddata");
            if(file.exists()){
                tessOCR = new TessOCR(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"");
                Toast.makeText(this,"Optical character recognition initialised", Toast.LENGTH_SHORT).show();
            }
        }).run();
    }

    /**
     * Save the image filename and mostRecent ocr result to restore the
     * UI after the activity got destroyed
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(FILENAME, imageFile.getAbsolutePath());
        outState.putString(OCR_RESULT, mostRecentOcrResult);
    }

    /**
     * restore the UI to its original state
     */
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String imageFilename = (String)savedInstanceState.get(FILENAME);
        imageFile = new File(imageFilename);
        uri = FileProvider.getUriForFile(this,
                "eu.janlueking.kanjiscanner.fileprovider", imageFile);
        Bitmap image = BitmapFactory.decodeFile(imageFilename);
        if (image != null) {
            updateImage(image);
        }
        String result = (String)savedInstanceState.get(OCR_RESULT);
        if (result != null) {
            mostRecentOcrResult = result;
            displayScanResults();
        }
    }

    /**
     * starts the tutorial activity
     */
    private void tutorialButtonClicked() {
        final Intent intent = new Intent(this, TutorialActivity.class);
        startActivity(intent);
    }

    /**
     * copies the mostRecentOCR result to the search bar
     */
    private void copyToSearchButtonClicked() {
        searchBar.setText(searchBar.getText() + mostRecentOcrResult);
        searchBar.setSelection(searchBar.getText().length());
    }

    /**
     * starts the kanjiListActivity
     */
    private void browseListsButtonClicked() {
        final Intent intent = new Intent(this, KanjiListsActivity.class);
        startActivity(intent);
    }

    /**
     * starts the chooser to pick from where to obtain an image.
     * Starts an activity using the cameraIntent or the galleryIntent
     */
    private void selectImageButtonClicked() {
        final Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        final Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
        final Intent cameraIntent = getCameraIntent();
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{cameraIntent});
        chooser.putExtra(Intent.EXTRA_TITLE, getString(R.string.chooser_text));
        try {
            startActivityForResult(chooser, PIC_REQUEST);
        }
        catch (ActivityNotFoundException e) {
            Toast.makeText(this,
                    R.string.no_camera_error_message,
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    /**
     * starts the result Activity to show the search results for
     * the query that was inserted to the search bar
     */
    private void searchButtonClicked() {
        String search_text = searchBar.getText().toString();
        final Intent intent = new Intent(this, ResultsActivity.class);
        intent.putExtra("kanji_string", search_text);
        if (wordsCheckBox.isChecked()) {
            intent.putExtra("show_words", true);
        }
        startActivity(intent);
    }

    /**
     * starts the about activity
     */
    private void aboutButtonClicked() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    private void settingsButtonClicked() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tessOCR.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            return;
        }
        switch(requestCode) {
            case PIC_REQUEST:
                processPicRequestResult(intent);
                break;
            case CROP_REQUEST:
                processCropRequestResults(intent);
                break;
        }
    }

    /**
     * creates the camera intent used to take an image. Also creates
     * an Image file where the taken Image will be stored
     * @return the camera intent
     */
    private Intent getCameraIntent() {
        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION |
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        return intent;
    }

    /**
     * Source: https://github.com/biokys/cropimage
     * displays the cropped image
     */
    private void processCropRequestResults(Intent data) {
        String path = data.getStringExtra(CropImage.IMAGE_PATH);
        // if nothing received
        if (path == null) {
            return;
        }
        // cropped bitmap
        Bitmap img = BitmapFactory.decodeFile(path);
        updateImage(img);
    }

    /**
     * sets the uri and the filename of the newly selected Image, displays
     * it in the UI and change the UI accordingly
     * @param intent the returned result intent
     */
    private void processPicRequestResult(Intent intent) {
        try {
            if (intent != null && intent.getData() != null) {
                // in case of choosing a file from the gallery
                // otherwise imagefile has already been set in
                // cameraIntent
                Uri galleryUri = intent.getData();
                loadToImageFileFromUri(galleryUri);
            }
            Bitmap img = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
            updateImage(img);
        }
        catch (IOException e) {
            Toast.makeText(this, R.string.get_image_error,
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    /**
     * do the ocr on the image stored in kanjiscan in a different thread and
     * change the UI to show a progressbar.
     */
    public void scanImageButtonClicked() {
        if (tessOCR == null){
            Toast.makeText(this, R.string.no_ocr_data_warning, Toast.LENGTH_LONG).show();
            return;
        }
        scanProgressBar.setVisibility(View.VISIBLE);
        imageDescriptionTextView.setText(getString(R.string.scanning));
        if (ocrThread == null || !ocrThread.isAlive()) {
            ocrThread = new Thread(() -> {
                runOcrThread(kanjiScan);
            });
            ocrThread.start();
        }
    }

    /**
     * do the optical character recognition and display the results
     * @param bitmap the input image for the ocr
     */
    private void runOcrThread(Bitmap bitmap) {
        if (mostRecentOcrResult == null) {
            String result = tessOCR.getOCRResult(bitmap);
            if (result != null) {
                mostRecentOcrResult = Helpers.filterAvailableKanji(result);
                displayScanResults();
            }
        }
    }

    /**
     * Source: https://github.com/biokys/cropimage
     * starts the simple crop image activity via an intent
     */
    private void runCropImage() {
        // create explicit intent
        Intent intent = new Intent(this, CropImage.class);

        // tell CropImage activity to look for image to crop
        intent.putExtra(CropImage.IMAGE_PATH, imageFile.getAbsolutePath());

        // allow CropImage activity to rescale image
        intent.putExtra(CropImage.SCALE, true);

        // variable aspect ratio
        intent.putExtra(CropImage.ASPECT_X, 0);
        intent.putExtra(CropImage.ASPECT_Y, 0);

        // start activity CropImage with certain request code and listen
        // for result
        startActivityForResult(intent, CROP_REQUEST);
    }

    /**
     * processes the bitmap, updates the ImageView with the new bitmap, assigns it to
     * kanjiscan, changes the UI, resets the mostRecentOCR result and stops the ocrThread
     * if it is still running
     * @param bitmap the new image
     */
    private void updateImage(Bitmap bitmap) {
        kanjiScan = ImageProcessor.doImagePreprocessing(bitmap);
        imageView.setImageBitmap(kanjiScan);
        imageView.setVisibility(View.VISIBLE);
        updateUINewImage();
        mostRecentOcrResult = null; // reset because there is a new image now
        if (ocrThread != null && ocrThread.isAlive()) {
            tessOCR.stop();
        }
    }

    /**
     * updates the UI when a new Image gets set
     */
    private void updateUINewImage() {
        imageDescriptionTextView.setVisibility(View.VISIBLE);
        imageDescriptionTextView.setText(R.string.preprocessed_image);
        scanImageButton.setVisibility(View.VISIBLE);
        copyToSearchButton.setVisibility(View.GONE);
        scanProgressBar.setVisibility(View.GONE);
        copyToSearchButton.setVisibility(View.GONE);
        cropButton.setVisibility(View.VISIBLE);
    }

    /**
     * Source: https://stackoverflow.com/a/45520771
     * author: GeekDroid (slightly modified by me)
     * loads the image obtained from the gallery intent
     * to imageFile
     */
    private void loadToImageFileFromUri(Uri uri) {
        try {
            InputStream in =  getContentResolver().openInputStream(uri);
            OutputStream out = new FileOutputStream(imageFile);
            byte[] buf = new byte[1024];
            int len;
            while((len=in.read(buf))>0){
                out.write(buf,0,len);
            }
            out.close();
            in.close();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * show the ocr result and update the UI
     */
    private void displayScanResults() {
        runOnUiThread(() -> {
            scanProgressBar.setVisibility(View.GONE);
            if (mostRecentOcrResult.length() > 0) {
                imageDescriptionTextView.setText(mostRecentOcrResult);
            }
            else {
                imageDescriptionTextView.setText(getString(R.string.no_kanji_found));
            }
            scanImageButton.setVisibility(View.GONE);
            copyToSearchButton.setVisibility(View.VISIBLE);
        });
    }

    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

            if (referenceId == Settings.jpnDownloadId) {
                startTesseractAPI();
            }
        }
    };
}

package eu.janlueking.kanjiscanner.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import eu.janlueking.kanjiscanner.data.Kanji;
import eu.janlueking.kanjiscanner.data.KanjiWord;
import eu.janlueking.kanjiscanner.dataThreads.GetKanjiObjListener;
import eu.janlueking.kanjiscanner.dataThreads.GetKanjiObjThread;
import eu.janlueking.kanjiscanner.dataThreads.GetKanjiWordsListener;
import eu.janlueking.kanjiscanner.dataThreads.GetKanjiWordsThread;
import eu.janlueking.kanjiscanner.R;
import eu.janlueking.kanjiscanner.utils.StringFormatter;
import eu.janlueking.kanjiscanner.listViewAdapter.WordsListAdapter;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Activity for displaying all data of one Kanji and all words containing that
 * Kanji
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class KanjiActivity extends AppCompatActivity implements GetKanjiObjListener,
        GetKanjiWordsListener {
    /** all words containing the kanji */
    private KanjiWord[] words;
    /** information about one kanji */
    private Kanji kanjiData;

    // global definition of Views for less duplicate code and shorter lines
    private TextView kunTextView;
    private TextView onTextView;
    private TextView meaningsTextView;
    private TextView kanjiTextView;
    private TextView unicodeTextView;
    private TextView gradeTextView;
    private TextView jlptTextView;
    private TextView strokeCountTextView;
    private TextView loadingWordsTextView;
    private Button reloadButton;
    private Button reloadWordsButton;
    private Button copyButton;
    private ListView wordsListView;
    private TextInputEditText searchWordsEditText;
    private Toast errorToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_kanji);

        initGlobalUIVariables();

        Bundle extras = getIntent().getExtras();
        kanjiData = getKanjiFromExtras(extras);

        kanjiTextView.setText(Character.toString(kanjiData.kanji));

        reloadButton.setOnClickListener(this::reloadButtonPressed);
        reloadWordsButton.setOnClickListener(this::reloadWordsButtonPressed);
        copyButton.setOnClickListener(view -> copyButtonPressed(kanjiData.kanji));

        searchWordsEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateWordsList(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (kanjiData.isFilled()) {
            displayKanjiData(kanjiData);
        }
        else {
            loadKanji();
        }

        loadWords();
    }

    /**
     * updates the words shown in the wordsListView according to the search query
     */
    private void updateWordsList(CharSequence searchQuery) {
        if(words == null) {
            return;
        }
        if (searchQuery.length() == 0) {
            wordsListView.setAdapter(new WordsListAdapter(this, words));
            return;
        }
        String searchString = searchQuery.toString();
        ArrayList<KanjiWord> filteredWords = new ArrayList<>();
        for (KanjiWord word : this.words) {
            if (word.written.contains(searchString) || word.pronounced.contains(searchString)) {
                filteredWords.add(word);
                break;
            }
            for (String meaning : word.meanings) {
                if (meaning.contains(searchString)) {
                    filteredWords.add(word);
                    break;
                }
            }
        }
        KanjiWord[] filteredWordsArray = filteredWords.toArray(new KanjiWord[0]);
        wordsListView.setAdapter(new WordsListAdapter(this, filteredWordsArray));
    }

    /**
     * starts an asynchronous thread that obtains the kanji data and passes it
     * to getKanjiObjCallback when finished
     */
    private void loadKanji() {
        setKanjiUILoading();
        GetKanjiObjThread objThread = new GetKanjiObjThread(this, kanjiData.kanji);
        objThread.start();
    }

    /**
     * starts an asynchronous thread that obtains the kanji words data and
     * passes it to getKanjiWordsCallback when finished
     */
    private void loadWords() {
        setWordsUILoading();
        GetKanjiWordsThread thread = new GetKanjiWordsThread(this, kanjiData.kanji);
        thread.start();
    }

    /**
     * fills the UI with the Kanji data obtained by calling loadKanji or displays errors
     * if the obtained Data is null
     */
    @Override
    public void getKanjiObjCallback(Kanji kanjiData) {
        runOnUiThread(() -> {
            if (kanjiData != null) {
                displayKanjiData(kanjiData);
            }
            else {
                setKanjiUIError();
            }
        });
    }

    /**
     * fills the ListView with the words obtained from calling loadWords or display
     * errors if the obtained data is null
     */
    @Override
    public void getKanjiWordsCallback(KanjiWord[] words) {
        runOnUiThread(() -> {
            if (words != null) {
                displayWordsData(words);
            }
            else {
                setWordsUIError();
            }
        });
    }

    /**
     * makes the UI indicate that it is loading data
     */
    private void setKanjiUILoading() {
        reloadButton.setVisibility(View.GONE);
        unicodeTextView.setText(getString(R.string.unicode, getString(R.string.dots)));
        gradeTextView.setText(getString(R.string.grade, getString(R.string.dots)));
        jlptTextView.setText(getString(R.string.jlpt, getString(R.string.dots)));
        strokeCountTextView.setText(getString(R.string.stroke_count, getString(R.string.dots)));
        kunTextView.setText(R.string.loading);
        onTextView.setText(R.string.loading);
        meaningsTextView.setText(R.string.loading);
    }

    /**
     * makes the words List indicate that it is loading data
     */
    private void setWordsUILoading() {
        reloadWordsButton.setVisibility(View.GONE);
        loadingWordsTextView.setVisibility(View.VISIBLE);
        loadingWordsTextView.setText(R.string.loading);
    }

    /**
     * makes the UI indicate that there was an error loading the data
     */
    private void setKanjiUIError() {
        errorToast.show();
        kunTextView.setText(R.string.error);
        onTextView.setText(R.string.error);
        meaningsTextView.setText(R.string.error);
        reloadButton.setVisibility(View.VISIBLE);
    }

    /**
     * makes the words List indicate that there was an error loading the data
     */
    private void setWordsUIError() {
        errorToast.show();
        loadingWordsTextView.setVisibility(View.VISIBLE);
        loadingWordsTextView.setText(R.string.error_loading_words);
        reloadWordsButton.setVisibility(View.VISIBLE);
    }

    /**
     * displays the kanji data in the UI
     */
    private void displayKanjiData(Kanji kanjiData) {
        this.kanjiData = kanjiData;
        kanjiTextView.setText(Character.toString(kanjiData.kanji));
        unicodeTextView.setText(getString(R.string.unicode, kanjiData.unicode));
        gradeTextView.setText(getString(R.string.grade, kanjiData.grade));
        jlptTextView.setText(getString(R.string.jlpt, kanjiData.jlpt));
        strokeCountTextView.setText(getString(R.string.stroke_count, ""+kanjiData.stroke_count));

        String kunReadingsString = StringFormatter.getCommaSeparatedString(kanjiData.kun_readings);
        kunTextView.setText(kunReadingsString);

        String onReadingsString = StringFormatter.getCommaSeparatedString(kanjiData.on_readings);
        onTextView.setText(onReadingsString);

        String meaningsString = StringFormatter.getCommaSeparatedString(kanjiData.meanings);
        meaningsTextView.setText(meaningsString);
    }

    /**
     * sorts the words and puts them into the words List
     */
    private void displayWordsData(KanjiWord[] words) {
        this.words = words;
        Arrays.sort(words, (o1, o2) -> o1.written.length() - o2.written.length());
        loadingWordsTextView.setVisibility(View.GONE);
        reloadWordsButton.setVisibility(View.GONE);
        wordsListView.setAdapter(new WordsListAdapter(this, words));
    }

    /**
     * copies the kanji character to the clipboard
     */
    private void copyButtonPressed(char kanji) {
        ClipboardManager clipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("kanji", Character.toString(kanji));
        clipboard.setPrimaryClip(clip);
        Toast.makeText(this, R.string.copied, Toast.LENGTH_SHORT).show();
    }

    /**
     * reloads the kanji data and words data when they have not been loaded
     * yet
     */
    private void reloadButtonPressed(View reloadButton) {
        loadKanji();
        if (words == null) {
            loadWords();
        }
    }

    /**
     * reloads the words data and kanji data when they have not been loaded
     * yet
     */
    private void reloadWordsButtonPressed(View reloadWordsButton) {
        loadWords();
        if (!kanjiData.isFilled()) {
            loadKanji();
        }
    }

    /**
     * init all global UI variables
     */
    private void initGlobalUIVariables() {
        kunTextView = findViewById(R.id.kun_readings);
        onTextView = findViewById(R.id.on_readings);
        meaningsTextView = findViewById(R.id.meanings);
        kanjiTextView = findViewById(R.id.kanji_text_view);
        unicodeTextView = findViewById(R.id.text_view_unicode);
        gradeTextView = findViewById(R.id.text_view_grade);
        jlptTextView = findViewById(R.id.text_view_jlpt);
        strokeCountTextView = findViewById(R.id.text_view_stroke_count);
        loadingWordsTextView = findViewById(R.id.loading_words_error_textview);
        reloadButton = findViewById(R.id.reload_button);
        reloadWordsButton = findViewById(R.id.reload_words_button);
        wordsListView = findViewById(R.id.words_list);
        copyButton = findViewById(R.id.copy_button);
        searchWordsEditText = findViewById(R.id.search_words_edit_text);
        errorToast = Toast.makeText(this,
                R.string.kanjiAPI_connection_error,
                Toast.LENGTH_SHORT);
    }

    /**
     * loads all attributes of the Kanji object out of the extras
     * and creates a kanji Object from it
     * @param   extras  the extras bundle
     * @return          the filled Kanji object
     */
    private Kanji getKanjiFromExtras(Bundle extras) {
        Kanji kanjiData = new Kanji();
        kanjiData.kanji = extras.getChar("kanji");
        kanjiData.grade = extras.getString("grade");
        kanjiData.jlpt = extras.getString("jlpt");
        kanjiData.kun_readings = extras.getStringArray("kun");
        kanjiData.on_readings = extras.getStringArray("on");
        kanjiData.name_readings = extras.getStringArray("name");
        kanjiData.meanings = extras.getStringArray("meanings");
        kanjiData.stroke_count = extras.getInt("stroke_count");
        kanjiData.unicode = extras.getString("unicode");
        return kanjiData;
    }
}

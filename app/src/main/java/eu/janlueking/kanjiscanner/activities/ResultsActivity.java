package eu.janlueking.kanjiscanner.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import eu.janlueking.kanjiscanner.data.Helpers;
import eu.janlueking.kanjiscanner.data.Kanji;
import eu.janlueking.kanjiscanner.data.KanjiWord;
import eu.janlueking.kanjiscanner.dataThreads.GetKanjiWordsListener;
import eu.janlueking.kanjiscanner.dataThreads.GetKanjiWordsThread;
import eu.janlueking.kanjiscanner.R;
import eu.janlueking.kanjiscanner.listViewAdapter.ResultsListAdapter;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This activity loads and displays kanji and potentially words containing said kanji
 * together in a listView. The kanji need to be passed as a String via putExtra using
 * the key "kanji_string". If words shall also be loaded and displayed you also need
 * to put true as an extra using the key "show_words"
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class ResultsActivity extends AppCompatActivity implements GetKanjiWordsListener {
    /** the kanji to be loaded and displayed */
    char[] kanji;

    /** the listAdapter for the listView */
    ResultsListAdapter listAdapter;

    /** the big ListView of this layout*/
    ListView resultsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_results);

        resultsListView = findViewById(R.id.result_list);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            displayNothingFound();
            return;
        }

        String kanjiString = extras.getString("kanji_string");
        char[] kanjiChars = kanjiString.toCharArray();
        kanji = Helpers.filterAvailableKanji(kanjiChars);
        if (kanji.length == 0) {
            displayNothingFound();
            return;
        }

        fillListViewWithKanji(kanji);

        resultsListView.setOnItemClickListener((parent, View, position, id) -> {
            onListViewItemClicked(position);
        });

        // start thread that gets words similar to the search query if desired
        boolean showWords = extras.getBoolean("show_words");
        if (showWords) {
            new GetKanjiWordsThread(this, kanji[0]).start();
        }
    }

    /**
     * fills all unique kanji into the ListView (the data for the kanji is
     * requested in the listAdapter)
     * @param kanjiChars the kanji to display in the ListView
     */
    private void fillListViewWithKanji(char[] kanjiChars) {
        char[] uniqueKanji = Helpers.filterUniqueCharacters(kanjiChars);
        listAdapter = new ResultsListAdapter(this, uniqueKanji);
        resultsListView.setAdapter(listAdapter);
    }

    /** starts KanjiActivity with selected kanji from ListView */
    private void onListViewItemClicked(int position) {
        if (listAdapter.getItemViewType(position) == 0) {
            startKanjiActivity(((Kanji)listAdapter.getItem(position)));
        }
    }

    /**
     * display that nothing was found in the UI
     */
    private void displayNothingFound() {
        TextView textview = (TextView)findViewById(R.id.nothing_found_text_view);
        textview.setVisibility(View.VISIBLE);
        resultsListView.setVisibility(View.GONE);
    }

    /**
     * filters the words so that only words remain that contain every kanji in the
     * search query to the same multiplicity (if the search query contains a kanji
     * twice, then only words that also contain that kanji twice will be selected)
     * It then orders the remaining words by their length and displays them in the
     * listView
     * @param words The KanjiWords List or null if the thread failed to get the List
     */
    @Override
    public void getKanjiWordsCallback(KanjiWord[] words) {
        if (words == null) {
            Toast.makeText(this,R.string.words_error,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        ArrayList<KanjiWord> fullyContainingWords = new ArrayList<>();

        String unique = new String(Helpers.filterUniqueCharacters(kanji));
        int[] multiplicity = new int[unique.length()];
        for (char c : kanji) {
            multiplicity[unique.indexOf(c)]++;
        }
        for (KanjiWord word : words) {
            int[] mul = multiplicity.clone();
            boolean fullyContaining = true;
            for (char c : word.written.toCharArray()) {
                int index = unique.indexOf(c);
                if (index == -1) {
                    continue;
                }
                mul[unique.indexOf(c)]--;
            }
            for (int m : mul) {
                if (m != 0) {
                    fullyContaining = false;
                    break;
                }
            }
            if (fullyContaining) {
                fullyContainingWords.add(word);
            }
        }
        KanjiWord[] fullyContainingWordsArray =
                fullyContainingWords.toArray(new KanjiWord[0]);
        Arrays.sort(fullyContainingWordsArray,
                (o1,o2) -> o1.written.length() - o2.written.length());
        runOnUiThread(() -> listAdapter.addKanjiWords(fullyContainingWordsArray));
    }

    /**
     * starts the Kanji Activity when a kanji Item in a ListView gets clicked
     * @param kanjiObj the kanji object of that ListViewItem
     */
    private void startKanjiActivity(Kanji kanjiObj) {
        final Intent intent = new Intent(this, KanjiActivity.class);
        putKanjiToIntent(intent, kanjiObj);
        startActivity(intent);
    }

    /**
     * puts all data of the kanji to the Intent so it does not need to be reloaded
     * in the KanjiActivity (if it has successfully been loaded here)
     * @param   intent      intent to put the data to
     * @param   kanjiObj    kanjiObj holding the data about a kanji
     */
    private void putKanjiToIntent(Intent intent, Kanji kanjiObj) {
        intent.putExtra("kanji", kanjiObj.kanji);
        if (kanjiObj.isFilled()) {
            intent.putExtra("grade", kanjiObj.grade);
            intent.putExtra("jlpt", kanjiObj.jlpt);
            intent.putExtra("kun", kanjiObj.kun_readings);
            intent.putExtra("on", kanjiObj.on_readings);
            intent.putExtra("name", kanjiObj.name_readings);
            intent.putExtra("meanings", kanjiObj.meanings);
            intent.putExtra("stroke_count", kanjiObj.stroke_count);
            intent.putExtra("unicode", kanjiObj.unicode);
        }
    }
}

package eu.janlueking.kanjiscanner.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import eu.janlueking.kanjiscanner.data.Helpers;
import eu.janlueking.kanjiscanner.R;

/**
 * Activity with buttons that each lead to predefined lists with Kanji
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class KanjiListsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Select List");
        setContentView(R.layout.activity_kanji_list);
        Button gradeOneButton = findViewById(R.id.grade_one_list_button);
        gradeOneButton.setOnClickListener(v -> {
            startResultsActivity("grade-1.json");
        });
        Button gradeTwoButton = findViewById(R.id.grade_two_list_button);
        gradeTwoButton.setOnClickListener(v -> {
            startResultsActivity("grade-2.json");
        });
        Button gradeThreeButton = findViewById(R.id.grade_three_list_button);
        gradeThreeButton.setOnClickListener(v -> {
            startResultsActivity("grade-3.json");
        });
        Button gradeFourButton = findViewById(R.id.grade_four_list_button);
        gradeFourButton.setOnClickListener(v -> {
            startResultsActivity("grade-4.json");
        });
        Button gradeFiveButton = findViewById(R.id.grade_five_list_button);
        gradeFiveButton.setOnClickListener(v -> {
            startResultsActivity("grade-5.json");
        });
        Button gradeSixButton = findViewById(R.id.grade_six_list_button);
        gradeSixButton.setOnClickListener(v -> {
            startResultsActivity("grade-6.json");
        });
        Button remainingJoyoButton = findViewById(R.id.remaining_joyo_button);
        remainingJoyoButton.setOnClickListener(v -> {
            startResultsActivity("grade-8.json");
        });
        Button n5Button = findViewById(R.id.jlpt_n5_button);
        n5Button.setOnClickListener(v -> {
            startResultsActivity("jlpt-n5.json");
        });
        Button n4Button = findViewById(R.id.jlpt_n4_button);
        n4Button.setOnClickListener(v -> {
            startResultsActivity("jlpt-n4.json");
        });
        Button n3Button = findViewById(R.id.jlpt_n3_button);
        n3Button.setOnClickListener(v -> {
            startResultsActivity("jlpt-n3.json");
        });
        Button n2Button = findViewById(R.id.jlpt_n2_button);
        n2Button.setOnClickListener(v -> {
            startResultsActivity("jlpt-n2.json");
        });
    }

    /**
     * start a ResultsActivity with all the Kanji from the given list
     * @param listFileName the filename of the json List from res folder
     */
    private void startResultsActivity(String listFileName) {
        final Intent intent = new Intent(this, ResultsActivity.class);
        intent.putExtra("kanji_string", Helpers.getKanjiList(listFileName));
        startActivity(intent);
    }
}
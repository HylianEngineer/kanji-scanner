package eu.janlueking.kanjiscanner.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import eu.janlueking.kanjiscanner.R;
import eu.janlueking.kanjiscanner.data.Settings;

import java.io.File;

/**
 * activity where one can download traineddata files for ocr
 * @author Jan Lüking
 * code inspired by these sources:
 * https://www.codeproject.com/articles/1112730/android-download-manager-tutorial-how-to-download,
 * https://stackoverflow.com/a/37821076
 * @version 2022-04-23
 */
public class SettingsActivity extends AppCompatActivity {
    DownloadManager downloadManager;
    ProgressBar horizontalTrainedDataProgressBar;
    TextView download1DoneTextView;
    Button downloadHorizontalTrainedDataButton;
    Button deleteHorizontalTrainedDataButton;
    Button cancelHorizontalTrainedDataButton;
    Handler handler = new Handler();
    private boolean isProgressCheckerRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Settings");
        setContentView(R.layout.activity_settings);
        downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        downloadHorizontalTrainedDataButton =
                findViewById(R.id.download_horizontal_trained_data_button);
        downloadHorizontalTrainedDataButton.setOnClickListener(view ->
                downloadHorizontalTrainedDataButtonClicked());
        deleteHorizontalTrainedDataButton =
                findViewById(R.id.delete_horizontal_trained_data_button);
        deleteHorizontalTrainedDataButton.setOnClickListener(view ->
                deleteHorizontalTrainedDataButtonClicked());
        horizontalTrainedDataProgressBar = findViewById(R.id.download_horizontal_trained_data_bar);
        download1DoneTextView = findViewById(R.id.download1_done_text);
        cancelHorizontalTrainedDataButton = findViewById(R.id.cancel_download_horizontal_trained_data_button);
        cancelHorizontalTrainedDataButton.setOnClickListener(v -> cancelHorizontalTrainedDataDownload());

        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(downloadReceiver, filter);


        File file = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/tessdata/jpn.traineddata");
        if(file.exists()) {
            DownloadStatus status = checkDownloadStatus(Settings.jpnDownloadId);
            switch (status){
                case DOWNLOADED:
                    setUIDownloaded();
                    break;
                case NOT_DOWNLOADED:
                    setUINotDownloaded();
                    break;
                case  DOWNLOADING:
                    setUIDownloading();
                    startProgressChecker();
                    break;
            }
        }
        else {
            setUINotDownloaded();
        }
    }

    private void downloadHorizontalTrainedDataButtonClicked() {
        Uri trainedDataUri = Uri.parse("https://github.com/tesseract-ocr/tessdata/raw/main/jpn.traineddata");
        String filename = "tessdata/jpn.traineddata";
        Settings.jpnDownloadId = DownloadData(trainedDataUri, filename);
    }

    private void deleteHorizontalTrainedDataButtonClicked(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                File file = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/tessdata/jpn.traineddata");
                if(file.exists()){
                    if(file.delete()){
                        setUINotDownloaded();
                    }
                }
            }
        });
        dialog.setNegativeButton("cancel", (d,i) -> {});
        dialog.setMessage(R.string.delete_download_warning);
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }

    private void cancelHorizontalTrainedDataDownload(){
        downloadManager.remove(Settings.jpnDownloadId);
        setUINotDownloaded();
    }

    private void setUIDownloaded(){
        horizontalTrainedDataProgressBar.setVisibility(View.GONE);
        download1DoneTextView.setVisibility(View.VISIBLE);
        downloadHorizontalTrainedDataButton.setVisibility(View.INVISIBLE);
        deleteHorizontalTrainedDataButton.setVisibility(View.VISIBLE);
        cancelHorizontalTrainedDataButton.setVisibility(View.INVISIBLE);
    }

    private void setUIDownloading(){
        horizontalTrainedDataProgressBar.setVisibility(View.VISIBLE);
        downloadHorizontalTrainedDataButton.setVisibility(View.INVISIBLE);
        cancelHorizontalTrainedDataButton.setVisibility(View.VISIBLE);
    }

    private void setUINotDownloaded(){
        downloadHorizontalTrainedDataButton.setVisibility(View.VISIBLE);
        deleteHorizontalTrainedDataButton.setVisibility(View.INVISIBLE);
        download1DoneTextView.setVisibility(View.GONE);
        cancelHorizontalTrainedDataButton.setVisibility(View.INVISIBLE);
        horizontalTrainedDataProgressBar.setVisibility(View.GONE);
    }

    private long DownloadData (Uri uri, String filename) {

        long downloadReference;

        // Create request for android download manager
        downloadManager = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(uri);

        //Setting title of request
        request.setTitle("jpn.traineddata");

        //Setting description of request
        request.setDescription("Downloading trained data for optical character recognition");

        //Set the local destination for the downloaded file to a path
        //within the application's external files directory
        request.setDestinationInExternalFilesDir(this, Environment.DIRECTORY_DOWNLOADS, filename);

        //Enqueue download and save into referenceId
        downloadReference = downloadManager.enqueue(request);

        setUIDownloading();

        startProgressChecker();

        return downloadReference;
    }

    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //check if the broadcast message is for our enqueued download
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

            if (referenceId == Settings.jpnDownloadId) {
                setUIDownloaded();
                stopProgressChecker();
            }
        }
    };

    private DownloadStatus checkDownloadStatus(long downloadId) {

        DownloadManager.Query ImageDownloadQuery = new DownloadManager.Query();
        //set the query filter to our previously Enqueued download
        ImageDownloadQuery.setFilterById(downloadId);

        //Query the download manager about downloads that have been requested.
        Cursor cursor = downloadManager.query(ImageDownloadQuery);
        if(cursor.moveToFirst()){
            int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
            int status = cursor.getInt(columnIndex);
            switch (status){
                case DownloadManager.STATUS_FAILED:
                    return DownloadStatus.NOT_DOWNLOADED;
                case DownloadManager.STATUS_PAUSED:
                case DownloadManager.STATUS_PENDING:
                case DownloadManager.STATUS_RUNNING:
                    return DownloadStatus.DOWNLOADING;
                case DownloadManager.STATUS_SUCCESSFUL:
                    return DownloadStatus.DOWNLOADED;
            }
        }
        return DownloadStatus.DOWNLOADED;
        }

     enum DownloadStatus{
        NOT_DOWNLOADED, DOWNLOADING, DOWNLOADED
    }

    /**
     * Source:https://stackoverflow.com/a/37821076
     * (slightly modified by me)
     */
    private void checkProgress() {
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(Settings.jpnDownloadId);
        Cursor cursor = downloadManager.query(query);
        if (!cursor.moveToFirst()) {
            cursor.close();
            return;
        }
        long totalBytes = cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
        long progress = cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
        horizontalTrainedDataProgressBar.setProgress((int)((progress*100)/totalBytes));
        cursor.close();
    }

    /**
     * Source: https://stackoverflow.com/a/37821076
     */
    private Runnable progressChecker = new Runnable() {
        @Override
        public void run() {
            try {
                checkProgress();
            } finally {
                handler.postDelayed(progressChecker, 1000);
            }
        }
    };

    /**
     * Source: https://stackoverflow.com/a/37821076
     */
    private void startProgressChecker() {
        if (!isProgressCheckerRunning) {
            progressChecker.run();
            isProgressCheckerRunning = true;
        }
    }

    /**
     * Source: https://stackoverflow.com/a/37821076
     */
    private void stopProgressChecker() {
        handler.removeCallbacks(progressChecker);
        isProgressCheckerRunning = false;
    }
}
package eu.janlueking.kanjiscanner.imageRecognition;

import android.graphics.Bitmap;
import android.util.Log;

/* Source:
https://sourceforge.net/projects/opencvlibrary/files/opencv-android/
Version: 3.4.3
Licence: Apache2*/
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

/**
 * class for preprocessing an Image to make optical character recognition
 * better
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class ImageProcessor {
    /**
     * apply a chain of processsing methods to create an easily readable
     * image for optical character recognition
     * @param   bitmap  the bitmap to process
     * @return          the processed Bitmap
     */
    public static Bitmap doImagePreprocessing(Bitmap bitmap) {
        Mat image = new Mat();
        Utils.bitmapToMat(bitmap, image);

        Mat resizedImage = resize(image);
        resizedImage = toGrayscale(resizedImage);
        resizedImage = binarizeOtsu(resizedImage);
        resizedImage = invertIfWhiteOnBlack(resizedImage);
        resizedImage = close(resizedImage);

        Bitmap finalBitmap = Bitmap.createBitmap(resizedImage.width(),
                resizedImage.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(resizedImage, finalBitmap);
        return finalBitmap;
    }

    /**
     * resizes an Image so that the height is exactly 500
     * while the aspect ratio stays the same
     * @param   image   the image to resize
     * @return          the resized Image
     */
    private static Mat resize(Mat image) {
        Mat resizedImage = new Mat();
        double height = 500;
        double scale = height / image.height();
        double resizedImageWidth = image.width();
        double resizedImageHeight = image.height();
        resizedImageWidth *= scale;
        resizedImageHeight *= scale;
        Size newSize = new Size(resizedImageWidth, resizedImageHeight);
        Imgproc.resize(image, resizedImage, newSize, Imgproc.INTER_LINEAR);
        return resizedImage;
    }

    /**
     * binarizes the image by comparing the grayscale values of single points
     * to their local surroundings
     * @param   image   the image to binarize
     * @return          the binary Image
     */
    private static Mat binarize(Mat image) {
        int blockSize = (image.height()/5);
        if (blockSize % 2 == 0) { // blockSize needs to be odd
            blockSize++;
        }
        Mat binaryImage = new Mat();
        final int BIAS = 30;
        Imgproc.adaptiveThreshold(image, binaryImage, 255,
                Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY,  blockSize, BIAS);
        return binaryImage;
    }

    private static Mat binarizeOtsu(Mat image) {
        Mat binaryImage = new Mat();
        Imgproc.threshold(image, binaryImage, 0, 255,
                Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);
        return binaryImage;
    }

    private static Mat invertIfWhiteOnBlack(Mat image) {
        Mat invertedImage = new Mat();
        List<Mat> channels = new ArrayList<>();
        Core.split(image, channels);
        Mat gHist = new Mat();
        float[] range = {0, 256};
        MatOfFloat histRange = new MatOfFloat(range);
        Imgproc.calcHist(channels, new MatOfInt(0), new Mat(), gHist,
                new MatOfInt(256), histRange, false);
        System.out.println(gHist.get(255,0)[0]);
        if (gHist.get(0,0)[0] > gHist.get(255,0)[0]) {
            Core.bitwise_not(image, invertedImage);
            return invertedImage;
        }
        return image;
    }

    /**
     * converts an image into a grayscale Image
     * @param   image   image to turn into grayscale
     * @return          the grayscale image
     */
    private static Mat toGrayscale(Mat image) {
        Mat grayscaleImage = new Mat();
        Imgproc.cvtColor(image, grayscaleImage, Imgproc.COLOR_BGR2GRAY);
        return grayscaleImage;
    }

    /**
     * this increases the contrast of the image and thus makes the
     * gap between rather white and rather black pixels bigger and
     * more consistent between different images. This reduces
     * the bias of the C value in the adaptive Threshhold method
     * towards said gap
     * @param   image   the image
     * @return          the image with increased contrast
     */
    private static Mat equalizeHist(Mat image) {
        Mat equalizedImage = new Mat();
        long size = (long)(image.size().width * image.size().height);
        double amountBlack = .01;
        double amountWhite = .1;
        double[] cumulativeHist = getCumulativeHist(image);
        int lowerBorder = 0;
        for (int i = 0; i < 256; i++) {
            if (cumulativeHist[i] / size >= amountBlack) {
                lowerBorder = i;
                break;
            }
        }
        int higherBorder = 255;
        for (int i = 1; i < 256; i++) {
            if ((size - cumulativeHist[255-i]) / size >= amountWhite) {
                higherBorder = 256-i;
                break;
            }
        }
        double scale = 1. * 255/(higherBorder - lowerBorder);
        image.convertTo(equalizedImage, -1, scale, -lowerBorder*scale);
        return equalizedImage;
    }

    /**
     * morphological closing operation. This basically removes
     * singular black structures smaller than 5x5 Pixel from a binary Image
     * @param   binImage    the binary Image
     * @return              the binary image after the closing operation
     */
    private static Mat close(Mat binImage) {
        Mat closedImage = new Mat();
        Mat struct = getDefaultStructuringElement();
        Imgproc.morphologyEx(binImage, closedImage, Imgproc.MORPH_CLOSE, struct);
        return closedImage;
    }

    /**
     * morphological opening operation. This fills holes in
     * the characters
     * @param   binImage    the binary Image
     * @return              the binary image after the opening operation
     */
    private static Mat open(Mat binImage) {
        Mat openedImage = new Mat();
        Mat struct = getDefaultStructuringElement();
        Imgproc.morphologyEx(binImage, openedImage, Imgproc.MORPH_OPEN, struct);
        return openedImage;
    }

    /**
     * returns a cumulative histogram of the image stored in Mat. A cumulative histogram has a
     * value y for every grayscale value x that stores the amount of pixels in the picture
     * whose grayscale value is >= x
     * @param   mat the grayscale image
     * @return      cumulative Histogram of the image from 0 to 255
     */
    private static double[] getCumulativeHist(Mat mat) {
        List<Mat> channels = new ArrayList<>();
        Core.split(mat, channels);
        Mat gHist = new Mat();
        float[] range = {0, 256};
        MatOfFloat histRange = new MatOfFloat(range);
        Imgproc.calcHist(channels, new MatOfInt(0), new Mat(), gHist,
                new MatOfInt(256), histRange, false);
        double[] cumulativeHist = new double[256];
        cumulativeHist[0] = gHist.get(0, 0)[0];
        for (int i = 1; i < 256; i++) {
            cumulativeHist[i] = cumulativeHist[i-1] + gHist.get(i,0)[0];
        }
        return cumulativeHist;
    }

    /**
     * @return structuring element with kernel size 5x5 and a centered anchor point
     */
    private static Mat getDefaultStructuringElement() {
        Size kernelSize = new Size(5,5);
        Point anchorPoint = new Point(2,2);
        return Imgproc.getStructuringElement(Imgproc.MORPH_RECT, kernelSize, anchorPoint);
    }
}

package eu.janlueking.kanjiscanner.imageRecognition;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.widget.Toast;

import androidx.annotation.WorkerThread;

/* Source:
https://github.com/rmtheis/tess-two
Library: tess-two
License: Apache2
Copyright 2011 Robert Theis
 */
import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *  This class encapsulates the Tesseract API for doing character recognition
 *  on japanese characters written left to right (as opposed to top to bottom and right to left
 *  which is how japanese is traditionally written)
 *  @author  Jan Lüking
 *  @version 2022-04-16
 */
public class TessOCR {
    public final TessBaseAPI mTess;
    public boolean returnNull;
    public Context context;

    /**
     * @param datapath the path that contains a folder named "tessdata" which contains the
     *                 traineddata files
     */
    public TessOCR(String datapath) {
        mTess = new TessBaseAPI();
        mTess.init(datapath, "jpn");
    }

    /**
     * does the optical character recognition on a bitmap and returns the result
     * as a string. This can be interrupted by a different thread by calling stop
     * which will lead to this method returning null
     * @param   bitmap  the bitmap to do the ocr on
     * @return          the ocr result
     */
    @WorkerThread
    public String getOCRResult(Bitmap bitmap) {
        mTess.setImage(bitmap);
        mTess.getHOCRText(0);
        if (returnNull) {
            returnNull = false;
            return null;
        }
        return mTess.getUTF8Text();
    }

    /**
     * interrupts the getOCR method when it is being executed in a
     * different thread
     */
    public void stop() {
        returnNull = true;
        mTess.stop();
    }

    public void onDestroy() {
        if (mTess != null) mTess.end();
    }
}

package eu.janlueking.kanjiscanner.listViewAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.view.LayoutInflater;

import eu.janlueking.kanjiscanner.dataThreads.GetKanjiObjThread;
import eu.janlueking.kanjiscanner.views.KanjiButton;
import eu.janlueking.kanjiscanner.R;
import eu.janlueking.kanjiscanner.utils.StringFormatter;
import eu.janlueking.kanjiscanner.listViewAdapter.utils.WordViewCreator;
import eu.janlueking.kanjiscanner.data.Kanji;
import eu.janlueking.kanjiscanner.data.KanjiWord;

/**
 * Adapter for the ListView in ResultsActivity
 */
public class ResultsListAdapter extends BaseAdapter {
    /** the activity that holds the listView of this adapter */
    private final Activity activity;
    /** the Kanji objects holding the data for TYPE_KANJI items of this ListView */
    private final Kanji[] items;
    /** the  KanjiWord objects holding the data for TYPE_WORD items of this ListView*/
    private KanjiWord[] wordItems;

    /** ViewType of items that display kanji */
    private static final int TYPE_KANJI = 0;
    /** ViewType of items that display words */
    private static final int TYPE_WORD = 1;
    /** ViewType of items that are separators */
    private static final int TYPE_SEPARATOR = 2;

    /**
     * @param activity  the Activity of the ListView of this Adapter
     * @param items     all kanji to be displayed in the ListView
     */
    public ResultsListAdapter(Activity activity, char[] items) {
        super();
        this.activity = activity;
        this.items = new Kanji[items.length];
        for(int i = 0; i < items.length; i++) {
            Kanji item = new Kanji();
            item.kanji = items[i];
            this.items[i] = item;
        }
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // no reusing of views because references between threads and
        // views will get messed up which leads to strange behaviour
        if (getItemViewType(position) == TYPE_KANJI) {
            view = getLayoutInflater().inflate(R.layout.kanji_preview, parent, false);

            // get the item
            Kanji item = (Kanji)getItem(position);

            // initialise the Kanji Button
            KanjiButton button = view.findViewById(R.id.element_preview_button);
            button.setKanji(item.kanji);

            // if the data for the item has not yet been loaded
            if(!item.isFilled()) {
                // start a Thread, that will get the kanji Object and fill the view
                View finalView = view;
                GetKanjiObjThread thread = new GetKanjiObjThread(kanji -> {
                    if(kanji != null) {
                        // fill item with data so it only needs to be loaded once
                        setItem(position, kanji);
                    }
                    activity.runOnUiThread(() -> {
                        fillView(finalView, kanji); // fill view with content
                    });
                }, item.kanji);
                // execute a different Thread for each View item
                thread.start();
            }
            else {
                fillView(view, item);
            }
        }
        else if (getItemViewType(position) == TYPE_WORD) {
            view = WordViewCreator.getWordView(activity, parent, (KanjiWord)getItem(position));
        }
        else if (getItemViewType(position) == TYPE_SEPARATOR) {
            view = getLayoutInflater().inflate(R.layout.separator, parent, false);
            TextView textView = view.findViewById(R.id.separator_textview);
            textView.setText((String)getItem(position));
        }
        return view;
    }

    @Override
    public int getCount() {
        if (wordItems == null) {
            return items.length + 1;
        }
        else {
            return items.length + wordItems.length + 2;
        }
    }

    @Override
    public Object getItem(int position) {
        if (position == 0) {
            return "kanji";
        }
        else if (position < items.length + 1) {
            return items[position - 1];
        }
        else if (position == items.length + 1) {
            return "words";
        }
        else {
            return wordItems[position - items.length - 2];
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public int getViewTypeCount() {
        return 3;
    }

    /**
     * @param   position    position of the item in this adapters ListView
     * @return              one of the three ViewTypes TYPE_KANJI, TYPE_WORD, TYPE_SEPARATOR
     */
    @Override
    public int getItemViewType(int position) {
        if (getItem(position).getClass() == Kanji.class) {
            return TYPE_KANJI;
        }
        else if (getItem(position).getClass() == KanjiWord.class){
            return TYPE_WORD;
        }
        else {
            return TYPE_SEPARATOR;
        }
    }

    /**
     * Adds words to the result List
     * @param words the words to add
     */
    public void addKanjiWords(KanjiWord[] words) {
        wordItems = words;
        notifyDataSetChanged();
    }

    /**
     * replaces an item from items by a new one
     * @param position  the position of the item in the ListView
     * @param item      the new item
     */
    private void setItem(int position, Kanji item) {
        if (getItemViewType(position) == 0) {
            items[position - 1] = item;
        }
    }

    /**
     * fills the list kanji_preview view with the Kanji data
     * @param kanjiPreview  the kanji_preview view
     * @param kanji         the kanji data to fill the view with
     */
    private void fillView(View kanjiPreview, Kanji kanji) {
        TextView kunTextView = kanjiPreview.findViewById(R.id.kun_readings_preview);
        TextView onTextView = kanjiPreview.findViewById(R.id.on_readings_preview);
        TextView meaningsTextView = kanjiPreview.findViewById(R.id.meanings_preview);
        if (kanji == null) {
            kunTextView.setText(R.string.error);
            onTextView.setText(R.string.error);
            meaningsTextView.setText(R.string.error);
            return;
        }
        String kunReadingsString = StringFormatter.getCommaSeparatedString(kanji.kun_readings);
        kunTextView.setText(kunReadingsString);

        String onReadingsString = StringFormatter.getCommaSeparatedString(kanji.on_readings);
        onTextView.setText(onReadingsString);

        String meaningsString = StringFormatter.getCommaSeparatedString(kanji.meanings);
        meaningsTextView.setText(meaningsString);
    }

    /**
     * gets the LayoutInflater for the context of the activity
     * @return the LayoutInflater
     */
    private LayoutInflater getLayoutInflater() {
        return (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
}

package eu.janlueking.kanjiscanner.listViewAdapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import eu.janlueking.kanjiscanner.listViewAdapter.utils.WordViewCreator;
import eu.janlueking.kanjiscanner.data.KanjiWord;

/**
 * Adapter for the words ListView in the KanjiActivity
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class WordsListAdapter extends BaseAdapter {
    Context context;
    KanjiWord[] items;

    public WordsListAdapter(Context context, KanjiWord[] items) {
        super();
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        KanjiWord word = getItem(position);
        return WordViewCreator.getWordView(context, parent, word);
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public KanjiWord getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}

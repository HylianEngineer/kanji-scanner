package eu.janlueking.kanjiscanner.listViewAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import eu.janlueking.kanjiscanner.R;
import eu.janlueking.kanjiscanner.data.Credit;

/**
 * List Adapter that holds the credits shown in the AboutActivity
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class CreditsListAdapter extends BaseAdapter {
    /** Context for the Layout inflater */
    public Context context;
    /** list of Credit items to display in the ListView */
    public Credit[] credits;

    public CreditsListAdapter(Context context, Credit[] credits) {
        this.context = context;
        this.credits = credits;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = getLayoutInflater().inflate(R.layout.credit, parent, false);
        }
        Credit item = getItem(position);
        String internetIcon = "";
        if (item.getUrl() != null) {
            internetIcon = " \uD83C\uDF10";
        }

        TextView header = convertView.findViewById(R.id.credit_header);
        header.setText(context.getString(item.getHeader(), internetIcon));

        TextView subtext = convertView.findViewById(R.id.credit_subtext);
        subtext.setText(item.getSubtext());

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getCount() {
        return credits.length;
    }

    @Override
    public Credit getItem(int position) {
        return credits[position];
    }

    /**
     * @return the layout inflater for the context of this class
     */
    private LayoutInflater getLayoutInflater() {
        return (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
}

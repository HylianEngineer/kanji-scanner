package eu.janlueking.kanjiscanner.listViewAdapter.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import eu.janlueking.kanjiscanner.views.KanjiButton;
import eu.janlueking.kanjiscanner.R;
import eu.janlueking.kanjiscanner.utils.StringFormatter;
import eu.janlueking.kanjiscanner.data.Helpers;
import eu.janlueking.kanjiscanner.data.KanjiWord;

/**
 * this class provides a function that creates Views from the word_preview layout so it
 * can be used in multiple ListView Adapters without duplicate code
 * @author  Jan Lüking
 * @version 2022-01-02
 */
public class WordViewCreator {
    /**
     * create the view to display information to words in ListViews. The View writes the
     * words in a way, so that every Kanji is a Button which can be clicked to start the
     * Kanji Activity of the Kanji.
     * @param   context Context for the Layout Inflater
     * @param   parent parent where the view will be contained in
     * @param   word word Object for all the data of the word
     * @return  the view to be used by an Adapter
     */
    public static View getWordView(Context context, ViewGroup parent, KanjiWord word) {
        LayoutInflater inflater = getLayoutInflater(context);
        View view = inflater.inflate(R.layout.word_preview, parent, false);

        LinearLayout wordLayout = view.findViewById(R.id.word_layout);
        fillWordLayout(wordLayout, word, inflater, context);

        TextView pronouncedLabel = view.findViewById(R.id.pronounced_label);
        pronouncedLabel.setText(word.pronounced);

        TextView meaningsLabel = view.findViewById(R.id.word_meanings);
        String text = StringFormatter.getCommaSeparatedString(word.meanings);
        meaningsLabel.setText(text);

        return  view;
    }

    /**
     * fills the given LinearLayout with KanjiButtons and TextViews to spell the word
     */
    private static void fillWordLayout(LinearLayout wordLayout, KanjiWord word,
                                       LayoutInflater inflater, Context context) {
        for (char c : word.written.toCharArray()) {
            if (Helpers.isAvailableKanji(c)) {
                addKanjiButtonToLayout(wordLayout, c, inflater);
            }
            else {
                addKanaToLayout(wordLayout, c, context);
            }
        }
    }

    /**
     * creates and inserts a kanji button into the linear layout
     */
    private static void addKanjiButtonToLayout(LinearLayout wordLayout, char kanji,
                                               LayoutInflater inflater) {
        KanjiButton button = (KanjiButton) inflater.
                inflate(R.layout.kanji_button, wordLayout, false);
        button.setKanji(kanji);
        wordLayout.addView(button);
    }

    /**
     * creates and inserts a textView with a hiragana or katakana character into the linear layout
     */
    private static void addKanaToLayout(LinearLayout wordLayout, char kana, Context context) {
        TextView textView = new TextView(context);
        textView.setText(Character.toString(kana));
        textView.setTextSize(16);
        textView.setTextColor(context.getColor(R.color.black));
        wordLayout.addView(textView);
    }

    /**
     * returns the layout inflater from a context
     */
    private static LayoutInflater getLayoutInflater(Context context) {
        return (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
}


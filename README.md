# About this App #
Japanese kanji characters are often hard to identify. This App lets you take a picture of a kanji and identifies it for you. For each Kanji, it provides the english meaning, readings and words containing the kanji. The language data is taken from kanjiapi.dev.

# Data Privacy #
No data is collected by the provider of this app. However, this app downloads content from https://kanjiapi.dev/ and Github. Check out their Privacy Policies to see, how they deal with your data.